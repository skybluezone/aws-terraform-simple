echo "Setting default 404 path"

Set-WebConfigurationProperty -Filter "/system.webServer/httpErrors/error[@statusCode='404']" -Location IIS:\Sites\Default -Name ResponseMode -Value ExecuteURL
Set-WebConfigurationProperty -Filter "/system.webServer/httpErrors/error[@statusCode='404']" -Location IIS:\Sites\Default -Name path -Value /index.html
