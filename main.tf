# Specify the provider and access details
provider "aws" {
  region = "${var.aws_region}"
}

# Create a VPC to launch our instances into
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

# Create a subnet to launch our instances into
resource "aws_subnet" "default" {
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
}

# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "terraform_poc"
  description = "Used in the terraform"
  vpc_id      = "${aws_vpc.default.id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 5986
    to_port     = 5986
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {

  instance_type = "t2.micro"
  #ami = "ami-90e10bf2"
  ami = "ami-cda842af"
  key_name = "ec2-key-pair-syd"
  vpc_security_group_ids = ["${aws_security_group.default.id}"]
  subnet_id = "${aws_subnet.default.id}"

  tags {
    DeployGroup = "angular-starter-web"
  }

/*RHEL
  provisioner "remote-exec" {
    inline = [
      "echo ${aws_instance.web.private_ip}",
      "sudo yum -y install zip",
      "sudo yum -y install unzip"
    ]
    connection {
      type     = "ssh"
      user     = "ec2-user"
      private_key = "${file("~/.ssh/ec2-key-pair-syd.pem")}"
    }
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u ec2-user --private-key ~/.ssh/ec2-key-pair-syd.pem -i '${aws_instance.web.public_ip},' playbook_rhel/provision.yml"
  }
*/
  provisioner "remote-exec" {
    inline = [
      "echo ${aws_instance.web.private_ip}"
    ]
    connection {
      type     = "winrm"
      user     = "ec2-user"
      password = "EC2@123"
      insecure = "true"
      https = "true"
      port = 5986
    }
  }
  provisioner "local-exec" {
    command = "ansible-playbook -i '${aws_instance.web.public_ip},' playbook_win/provision.yml"
  }
}

terraform {
  backend "s3" {
    bucket = "skybluezone-terraform-state"
    key    = "simple/terraform.tfstate"
    region = "ap-southeast-2"
  }
}
