variable "key_name" {
  default   = "ec2-key-pair-syd"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "ap-southeast-2"
}
